# Gitlab Services

## Setup locally with `kind`

1. Create a kind cluster

    ```sh
    kind create cluster --config=kind.yml
    ```

1. Bootstrap flux

    ```sh
    export GITLAB_TOKEN=<your token> # Must have scopes `api` and `write_repository`
    flux bootstrap gitlab --owner alejandro --repository gitlab-services --path=clusters/kind --components-extra=image-reflector-controller,image-automation-controller --token-auth
    ```
